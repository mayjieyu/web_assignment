$(document).ready(function() {
    $.ajax({
        url:"https://sporadic.nz/2018a_web_assignment_service/Articles", //发送后台的url
        type:'get',
        beforeSend:function(XMLHttpRequest){
            $("#content01").html("<p> loading... </p>"); //在后台返回success之前显示loading图标
            $("#img1").html("<p> loading... </p>");

            $("#content02").html("<p> loading... </p>");
            $("#img2").html("<p> loading... </p>");

            $("#content03").html("<p> loading... </p>");
            $("#img3").html("<p> loading... </p>");

            $("#content04").html("<p> loading... </p>");
            $("#img4").html("<p> loading... </p>");

            $("#content05").html("<p> loading... </p>");
            $("#img5").html("<p> loading... </p>");
        },
        success:function(result){  //data为后台返回的数据
            var articleOne = result[0];
            var articleTwo = result[1];
            var articleThree = result[2];
            var articleFour = result[3];
            var articleFive = result[4];

            $(".title1").html( articleOne.title );
            $("#content01").html( articleOne.content );
            $("#img1").attr("src", articleOne.imageUrl);

            $(".title2").html( articleTwo.title );
            $("#content02").html( articleTwo.content );
            $("#img2").attr("src", articleTwo.imageUrl);

            $(".title3").html( articleThree.title );
            $("#content03").html( articleThree.content );
            $("#img3").attr("src", articleThree.imageUrl);

            $(".title4").html( articleFour.title );
            $("#content04").html( articleFour.content );
            $("#img4").attr("src", articleFour.imageUrl);

            $(".title5").html( articleFive.title );
            $("#content05").html( articleFive.content );
            $("#img5").attr("src", articleFive.imageUrl);
        }
    });
});